const {AuthenticationError, ForbiddenError} = require('apollo-server');
// import {AuthenticationError} from 'apollo-server';
import {ObjectId} from "promised-mongo";

const { query } = require('nact');

const resource = "solution";

module.exports = {

    Mutation:{
        changeSolution: async (obj, args, ctx, info) => {

            const collectionItemActor = ctx.children.get("item");

            if(args._id){
                return await query(collectionItemActor,  {"type": "solution", search:{_id: args._id}, input: args.input }, global.actor_timeout);
            }else {
                return await query(collectionItemActor, {"type": "solution", input: args.input}, global.actor_timeout);
            }

        },
    },

    Query: {

        getSolution: async (obj, args, ctx, info) => {


            const collectionItemActor = ctx.children.get("item");

            return await query(collectionItemActor, {"type": "solution", search: {_id: args.id}}, global.actor_timeout);


        },

        getSolutions: async (obj, args, ctx, info) => {

            const collectionActor = ctx.children.get("collection")

            return await query(collectionActor, {"type": "solution"}, global.actor_timeout);

        },
    },
}